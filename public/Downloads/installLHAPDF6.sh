#!/bin/bash

set_environment () {

  echo " Set environment variables"

  # Here, define your installation paths, versions etc.
  gccversion="$(gcc -dumpversion)"
  BOOST="$1"
  INSTALLD="$2"
  VERSION="$3"
  LOCAL=$INSTALLD
  export LHAPATH=$LOCAL/share/LHAPDF
  export LD_LIBRARY_PATH=$BOOST/lib:$LD_LIBRARY_PATH

}

run () {

  workd=$(pwd)

  echo " Download LHAPDF $VERSION"
  mkdir $INSTALLD
  cd $INSTALLD
  wget http://www.hepforge.org/archive/lhapdf/LHAPDF-${VERSION}.tar.gz

  echo " Unpack LHAPDF"
  tar xvzf LHAPDF-${VERSION}.tar.gz

  echo " Enter LHAPDF directory"
  cd LHAPDF-${VERSION}/

  echo " Configure LHAPDF"
  ./configure CXXFLAGS="-static-libstdc++" --prefix=$LOCAL --bindir=$LOCAL/bin \
  --datadir=$LOCAL/share --libdir=$LOCAL/lib --disable-python \
  --with-boost=$BOOST --enable-static

  echo " Compile LHAPDF"
  make

  echo " Install LHAPDF"
  make install

  echo "copy index and conf file"
  cd $INSTALLD
  index="$(find . -name 'pdfsets.index')"
  cp $index $INSTALLD/share/LHAPDF/
  conf="$(find . -name 'lhapdf.conf')"
  cp $conf $INSTALLD/share/LHAPDF/

  echo " Get LHAPDF sets"
  cd $INSTALLD/share/LHAPDF

  # Get all PDF sets!
  #wget --no-parent --recursive --level=1 -e robots=off -A.tar.gz -nd https://www.hepforge.org/archive/lhapdf/pdfsets/6.1/

  # Get CT10nlo
  wget https://www.hepforge.org/archive/lhapdf/pdfsets/6.1/CT10nlo.tar.gz

  ls -1 *.tar.gz | while read line; do tar xvfz $line; done

  echo " Finished LHAPDF installation"
  cd $workd

}

set_environment "$@"
run "$@"

