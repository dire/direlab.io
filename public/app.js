var url    = window.location.pathname;
var level0 = ['index.html',
              'Dire - simulating radiation cascades for particle physics'];
// one level down from home: [html file, print title]
var level1 = [ ['Downloads.html', 'Downloads'],
               ['Installation.html', 'Installation'],
               ['Documentation.html', 'Documentation'],
               ['Docker.html', 'Docker'] ];
// two levels down from home: [parent directory, html file, print title]
var level2 = [ ['Documentation', 'CodeStructure.html', 'Code Structure'],
               ['Documentation', 'Splittings.html', 'Splittings'],
               ['Documentation', 'SplittingLibrary.html', 'Splitting Library'],
               ['Documentation', 'DireTimes.html', 'Timelike evolution'],
               ['Documentation', 'DireSpace.html', 'Spacelike evolution'],
               ['Documentation', 'Docu2001.html', '2.001']];

// check where we are
var isActive = level0;
for (var i=0; i<level1.length; i++)
  if (url.includes(level1[i][0])) isActive = level1[i];
for (var i=0; i<level2.length; i++)
  if (url.includes(level2[i][0]+'/'+level2[i][1])) isActive = level2[i];
var isActiveLevel0 = (isActive == level0);
var isActiveLevel1 = (!isActiveLevel0 && isActive.length == 2);
var isActiveLevel2 = (!isActiveLevel0 && isActive.length == 3);

// INSERT NAVIGATION BAR AT THE SIDE

var sidenavbar = '<div class="sidenavbar">Contents: <br>';
if (isActiveLevel0 || isActiveLevel1) {
  sidenavbar += '&nbsp; &nbsp; <a href="index.html">Home</a> <br>';
  sidenavbar += '&nbsp; &nbsp; <a href="Downloads.html">Downloads</a> <br>';
  sidenavbar += '&nbsp; &nbsp; <a href="Installation.html">Installation</a> <br>';
  sidenavbar += '&nbsp; &nbsp; <a href="Usage.html">Usage</a> <br>';
  sidenavbar += '&nbsp; &nbsp; <a href="https://gitlab.com/dire/direforpythia">Repository</a> <br>';
  sidenavbar += '&nbsp; &nbsp; <a href="Documentation.html">Documentation</a> <br>';
  sidenavbar += '&nbsp; &nbsp; <a href="Docker.html">Docker</a>';
}
if (isActiveLevel2) {
  sidenavbar += '&nbsp; &nbsp; <a href="../index.html">Home</a> <br>';
  sidenavbar += '&nbsp; &nbsp; <a href="../Downloads.html">Downloads</a> <br>';
  sidenavbar += '&nbsp; &nbsp; <a href="../Usage.html">Usage</a> <br>';
  sidenavbar += '&nbsp; &nbsp; <a href="../Installation.html">Installation</a> <br>';
  sidenavbar += '&nbsp; &nbsp; <a href="https://gitlab.com/dire/direforpythia">Repository</a> <br>';
  sidenavbar += '&nbsp; &nbsp; <a href="../Documentation.html">Documentation</a> <br>';
  sidenavbar += '&nbsp; &nbsp; <a href="../Docker.html">Docker</a>';
}
sidenavbar += '</div>';

// add navigation bar to the top of the body
document.getElementsByTagName('body').item(0).insertAdjacentHTML('afterbegin',
  sidenavbar);

// INSERT NAVIGATION BAR AT THE TOP

// opening tag
var topnavbar = '<div class="topnavbar">';

// add home to navigation bar
if (isActiveLevel0) topnavbar += '<a class="active" href="'+level0[0]+'">';
if (isActiveLevel1) topnavbar += '<a href="'+level0[0]+'">';
if (isActiveLevel2) topnavbar += '<a href="../'+level0[0]+'">';
topnavbar += level0[1]+'</a>';

// add first level to navigation bar
if (isActiveLevel1)
  topnavbar += '<a class="active" href="'+isActive[0]+'">'+isActive[1]+'</a>';
if (isActiveLevel2) {
  for (var i=0; i<level1.length; i++)
    if (level1[i][1] == isActive[0])
      topnavbar += '<a href="../'+level1[i][0]+'">'+level1[i][1]+'</a>';
}

// add second level to navigation bar
if (isActiveLevel2)
  topnavbar += '<a class="active" href="'+isActive[0]+'/'+isActive[1]+'">'+
            isActive[2]+'</a>';

// closing tag
topnavbar += '</div>';

// add navigation bar to the top of the body
document.getElementsByTagName('body').item(0).insertAdjacentHTML('afterbegin',
  topnavbar);

// INSERT LAST EDITED AT THE BOTTOM

const monthNames = ["January", "February", "March", "April", "May", "June",
  "July", "August", "September", "October", "November", "December"];
var dLM   = new Date(document.lastModified);
var year  = dLM.getFullYear();
var month = monthNames[dLM.getMonth()];
var day  = dLM.getDate();
if (day<10) day = "0"+day;
document.getElementsByTagName('body').item(0).insertAdjacentHTML('beforeend',
  '<div class="lastedited">Last Edited: '+month+' '+day+', '+year+'</div>');
