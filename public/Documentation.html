<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Documentation</title>
    <link rel="stylesheet" href="style.css">
    <link rel="shortcut icon" href="icons/dire-logo-3.png"/>
  </head>
  <body>

    <h1>Documentation</h1>

    <p>
      Thanks for your interest in DIRE (short for dipole resummation), a
      <code>C++</code> program for all-order radiative
      corrections to scattering processes in high-energy particle collisions.
      The Dire parton shower combines dipole-shower ideas in the spirit of
      <a href="http://www.sciencedirect.com/science/article/pii/001046559290068A">
      Ariadne</a> with the careful handling of collinear enhancements in
      classical parton showers. As such, it is a full-fledged replacement of
      the Pythia 8 parton showers for lepton-lepton, lepton-hadron and
      hadron-hadron collisions, including a detailed treatment of mass effects.
      The Dire physics publication is
      <a href="http://arxiv.org/abs/1506.05057">arxiv:1506.05057 [hep-ph]</a>.
      Please cite this article (and of course the
      <a href="http://www.sciencedirect.com/science/article/pii/S0010465515000442">
      Pythia 8 reference</a>) if you use Pythia 8 + Dire for your research.
    </p>

    <h2>Tutorial</h2>

    <p>
      If you are eager to get started with Dire, you can check out this
      beginner's tutorial:
      <a href="Documentation/worksheet1500.pdf">worksheet1500.pdf</a>.
      Note that this tutorial was designed for version 1.500, and needs to be
      adjusted for later versions.
    </p>

    <h2>Input settings</h2>

    <p>
      The Dire plugin for Pythia 8 is implemented in accorrdance with the rules
      of implementing new parton showers for Pythia 8. Thus, the code
      structure is very similar to Pythia 8. Also, (almost) all input settings
      are reclycled from Pythia 8. In the following, we will first document
      Dire-specific input settings and then discuss the code structure.
    </p>

    <p>
      You can find detailed descriptions of settings and code here:
    </p>

    <ul>
      <li>
        <a href="Documentation/DireTimes.html">Dire Timelike evolution</a>:
        Settings related to timelike showers (final state radiation).
      </li>
      <li>
        <a href="Documentation/DireSpace.html">Dire Spacelike evolution</a>:
        Settings related to spacelike showers (initial state radiation).
      </li>
      <li>
        <a href="Documentation/CodeStructure.html">Code structure</a>:
        Description of the code structure, how to implement new features,
        and more.
      </li>
      <li>
        <a href="Documentation/Docu2001.html">Versions 2.001 and before</a>:
        Documentation for versions before 2.002.
      </li>
    </ul>

    <p>
      Dire currently has only very few switches, so that a very sophisticated
      documentation is not yet necessary. Below, we list some Dire-specific
      handles for quick reference. <i>All other settings are taken from
      Pythia 8, keeping their meaning. The Pythia 8 documentation can be found
      on the <a href="http://home.thep.lu.se/~torbjorn/Pythia.html">Pythia
      homepage</a>.</i>
    </p>

    <p>
      Settings related to the accuracy of splitting kernels used in the
      evolution:
    </p>

    <ul>
      <li>
        <b>DireTimes:kernelOrder = n</b> : <i>n</i> is an integer value, and
        set to 1 by default. This setting defines which higher-order
        corrections are applied to the parton-shower splitting functions used
        for timelike (i.e. final state) evolution:
        <ul>
          <li>
            <i>n=0</i>: Leading order evolution kernels, as defined in the
            original Dire publication. 
          </li>
          <li>
            <i>n=1</i>: Leading order evolution kernels, and additional
            rescaling of the soft pieces to incorporate two-loop cusp efects
            (similar to the CMW scheme).
          </li>
          <li>
            <i>n=2</i>: Leading order evolution kernels, and additional
            rescaling of the soft pieces to incorporate two- and three-loop
            cusp efects.
          </li>
          <li>
            <i>n=3</i>: Leading order evolution kernels, additional rescaling
            of the soft pieces   to incorporate two- and three-loop cusp
            efects, and NLO corrections to collinear evolution from NLO DGLAP
            kernels. 
          </li>
        </ul>
      </li>

      <li>
        <b>DireSpace:kernelOrder = n</b> : <i>n</i> is an integer value, and
        set to 1 by default. This setting defines which higher-order
        corrections are applied to the parton-shower splitting functions used
        for spacelike (i.e. initial state) evolution:
        <ul>
          <li>
            <i>n=0</i>: Leading order evolution kernels, as defined in the
            original Dire publication. 
          </li>
          <li>
            <i>n=1</i>: Leading order evolution kernels, and additional
            rescaling of the soft pieces to incorporate two-loop cusp efects
            (similar to the CMW scheme).
          </li>
          <li>
            <i>n=2</i>: Leading order evolution kernels, and additional
            rescaling of the soft pieces to incorporate two- and three-loop
            cusp efects.
          </li>
          <li>
            <i>n=3</i>: Leading order evolution kernels, additional rescaling
            of the soft pieces   to incorporate two- and three-loop cusp
            efects, and NLO corrections to collinear evolution from NLO DGLAP
            kernels. 
          </li>
        </ul>
      </li>

      <li>
        <b>DireTimes:kernelOrderMPI = n</b> : Same as
        <b>DireTimes:kernelOrder = n</b>, but for showers from secondary
        scatterings or hadron decays.
      </li>

      <li>
        <b>DireSpace:kernelOrderMPI = n</b> : Same as 
        <b>DireSpace:kernelOrder = n</b>, but   for showers from secondary
        scatterings or hadron decays.
      </li>

      <li>
        <b>DireTimes:pTrecombine = t</b> : The minimal pT value <i>t</i> (in
        GeV) of final-state emissions below which the splitting probabilities
        of some kernels are combined into a single splitting probability.
        To be more precise, for <i>pT&lt;t</i>, the probabilities for <i>q->qg</i>
        and <i>q->gq</i> are combined into a single probability, as are
        <i>g->qqbar</i> and <i>g->qbarq</i> and the two color structures for 
        <i>g->gg</i>. This can help improve the numerical stability of the
        Sudakov veto algorithm, however at the expense accuracy. Negative
        values mean that variations are performed for all parton shower
        emissions. Default value is 2.0 GeV.
      </li>
    </ul>

    <p>
      Settings related to the evaluation of running (QCD) couplings:
    </p>

    <ul>
      <li>
        <b>ShowerPDF:usePDFalphas = on/off</b> : This switch is turned off
        by default. Turned on, the alphaS running and thresholds will be
        directly taken from the PDF set interfaced through LHAPDF6. This can be
        helpful when validating the code, but will lead to a longer run time.
      </li>
    </ul>

    <p>
      Settings to perform variations to gauge shower uncertainties:
    </p>

    <ul>
      <li>
        <b>Variations:doVariations = on/off</b> : This switch is turned off by
        default. Turned on, this means that the timelike and spacelike showers
        are allowed to perform on-the-fly variations of the renormalization
        scale. Renormalization scale uncertainties are an important part of the
        perturbative evolution. Note that currently, these variations will also
        apply to showers off (soft) secondary scatterings. The range of
        renormalization scale variations is given by the next four parameters.
      </li>

      <li>
        <b>Variations:muRisrDown = k</b> : The (double) value with which the
        (GeV<sup>2</sup>-valued) argument of &alpha;<sub>s</sub> in
        initial-state evolution should be rescaled to produce a smaller value
        of the renormalization scale in the context of automatic variations.
      </li>

      <li>
        <b>Variations:muRisrUp = k</b> : The (double) value with which the
        (GeV<sup>2</sup>-valued) argument of &alpha;<sub>s</sub> in
        initial-state evolution should be rescaled to produce a larger value
        of the renormalization scale in the context of automatic variations.
      </li>

      <li>
        <b>Variations:muRfsrDown = k</b> : The (double) value with which the
        (GeV<sup>2</sup>-valued) argument of &alpha;<sub>s</sub> in
        final-state evolution should be rescaled to produce a smaller value
        of the renormalization scale in the context of automatic variations.
      </li>

      <li>
        <b>Variations:muRfsrUp = k</b> : The (double) value with which the
        (GeV<sup>2</sup>-valued) argument of &alpha;<sub>s</sub> in
        final-state evolution should be rescaled to produce a larger value
        of the renormalization scale in the context of automatic variations.
      </li>

      <li>
        <b>Variations:pTmin = t</b> : The minimal pT value <i>t</i> (in GeV)
        of emissions below which no parton shower variations are considered.
        Negative values mean that variations may be performed for emissions at
        any pT. Default value is -1.0 GeV.
      </li>
    </ul>

    <p>
      Settings related to parton distribution functions and quark masses:
    </p>

    <ul>
      <li>
        <b>ShowerPDF:usePDFmasses = on/off</b> : This switch is turned on by
        default. Turned on, quark masses will be directly taken from the PDF
        set interfaced through LHAPDF6. 
      </li>

      <li>
        <b>ShowerPDF:useSummedPDF = on/off</b> : This switch is turned on by
        default. Turned on, this means that the PDF ratios that are used in the
        evolution once an initial state parton partakes in a branching include
        both sea and valence quark contributions (if applicable).
      </li>

      <li>
        <b>DireSpace:useGlobalMapIF = on/off</b> : This switch is turned off by
        default. Turned on, this means that the phase space of intial state
        emissions with a final state spectator is setup such that all final
        state particles share the momentum recoil of the emission.
      </li>

      <li>
        <b>DireSpace:forceMassiveMap = on/off</b> : This switch is turned off
        by default. Turned on, this means that initial state emissions are
        allowed masses. This means the shower produces the awkward situation
        that incoming quarks are massless, but yield a assive final state
        quark upon conversion to an incoming gluon.
      </li>
    </ul>

    <p>
      Settings related to the tune of Pythia 8 + Dire:
    </p>

    <ul>
      <li>
        <b>Dire:Tune = n</b> : <i>n</i> is an integer value, and is set to 1
        by default. If set to one, this enables the default tune of
        Pythia8 + Dire. Currently, all other values mean that no Pythia
        parameters are automatically overwritten by Dire.
      </li>
    </ul>

    <p>
      Settings related to enhancements of emission probabilities to improve
      efficiency:
    </p>

    <ul>
      <li>
        <b>Enhance:pTmin = t</b> : The minimal pT value <i>t</i> (in GeV) of
        emissions below which no enhancement of emission probabilities is
        applied. Negative values mean that enhancements may be applied to
        emissions at any pT. Default value is -1.0 GeV.
      </li>

      <li>
        <b>Enhance:Dire_fsr_qcd_1->1&21_CS = k</b> : The (double) value with
        which the final-state <i>q->qg</i> splitting is enhanced. Note that
        the enhancement is compensated by an additional weight factor included
        in the parton shower weight, to guarantee the correct real-emission and
        no-emission probabilities are generated.
      </li>

      <li>
        <b>Other optional enhancements</b> : Dire offers the option to enhance
        each available splitting kernel separately. For brevity, we here only
        list the corresponding keywords:
        <ul>
          <li>
            <b>QCD splittings</b>:  <br>
            Enhance:Dire_fsr_qcd_1->1&21_CS	(FSR, q -> q g), <br>
            Enhance:Dire_fsr_qcd_1->21&1_CS	(FSR, q -> g q), <br>
            Enhance:Dire_fsr_qcd_21->21&21a_CS (FSR, g -> g g, 1st color structure), <br>
            Enhance:Dire_fsr_qcd_21->21&21b_CS (FSR, g -> g g, 2nd color structure), <br>
            Enhance:Dire_fsr_qcd_21->1&1a_CS (FSR, g -> q qbar, soft quark), <br>
            Enhance:Dire_fsr_qcd_21->1&1b_CS (FSR, g -> q qbar, soft antiquark), <br>
            Enhance:Dire_fsr_qcd_1->2&1&2_CS (FSR, q -> q' q qbar'), <br>
            Enhance:Dire_fsr_qcd_1->1&1&1_CS (FSR, q -> qbar q qbar), <br>
            Enhance:Dire_fsr_qcd_1->1&21_notPartial (FSR q -> q g, with full soft logarithms, as e.g. needed in radiation off top decay products), <br>
            Enhance:Dire_fsr_qcd_21->21&21_notPartial (FSR g -> g g, with full soft logarithms, as e.g. needed in radiation off top decay products), <br>
            Enhance:Dire_fsr_qcd_21->1&1_notPartial (FSR g -> q qbar, with full soft logarithms, as e.g. needed in radiation off top decay products), <br>
            Enhance:Dire_fsr_qcd_1->1&21&21_CS (FSR q -> q g g), <br>
            Enhance:Dire_fsr_qcd_1->1&d&dbar_CS (FSR q -> q d dbar, soft quark), <br>
            Enhance:Dire_fsr_qcd_1->1&dbar&d_CS (FSR q -> q d dbar, soft antiquark), <br>
            Enhance:Dire_fsr_qcd_1->1&u&ubar_CS (FSR q -> q u ubar, soft quark), <br>
            Enhance:Dire_fsr_qcd_1->1&ubar&u_CS (FSR q -> q u ubar, soft antiquark), <br>
            Enhance:Dire_fsr_qcd_1->1&s&sbar_CS (FSR q -> q s sbar, soft quark), <br>
            Enhance:Dire_fsr_qcd_1->1&sbar&s_CS (FSR q -> q s sbar, soft antiquark), <br>
            Enhance:Dire_fsr_qcd_1->1&c&cbar_CS (FSR q -> q c cbar, soft quark), <br>
            Enhance:Dire_fsr_qcd_1->1&cbar&c_CS (FSR q -> q c cbar, soft antiquark), <br>
            Enhance:Dire_fsr_qcd_1->1&b&bbar_CS (FSR q -> q b bbar, soft quark), <br>
            Enhance:Dire_fsr_qcd_1->1&bbar&b_CS (FSR q -> q b bbar, soft antiquark), <br>
            Enhance:Dire_fsr_qcd_21->21&21&21_CS (FSR g -> g g g), <br>
            Enhance:Dire_fsr_qcd_21->21&d&dbar_CS (FSR g -> q d dbar, soft quark), <br>
            Enhance:Dire_fsr_qcd_21->21&dbar&d_CS (FSR g -> g d dbar, soft antiquark), <br>
            Enhance:Dire_fsr_qcd_21->21&u&ubar_CS (FSR g -> g u ubar, soft quark), <br>
            Enhance:Dire_fsr_qcd_21->21&ubar&u_CS (FSR g -> g u ubar, soft antiquark), <br>
            Enhance:Dire_fsr_qcd_21->21&s&sbar_CS (FSR g -> g s sbar, soft quark), <br>
            Enhance:Dire_fsr_qcd_21->21&sbar&s_CS (FSR g -> g s sbar, soft antiquark), <br>
            Enhance:Dire_fsr_qcd_21->21&c&cbar_CS (FSR g -> g c cbar, soft quark), <br>
            Enhance:Dire_fsr_qcd_21->21&cbar&c_CS (FSR g -> g c cbar, soft antiquark), <br>
            Enhance:Dire_fsr_qcd_21->21&b&bbar_CS (FSR g -> g b bbar, soft quark), <br>
            Enhance:Dire_fsr_qcd_21->21&bbar&b_CS (FSR g -> g b bbar, soft antiquark), <br>
            Enhance:Dire_isr_qcd_1->1&21_CS (ISR q -> q g), <br>
            Enhance:Dire_isr_qcd_21->1&1_CS (ISR g -> q qbar), <br>
            Enhance:Dire_isr_qcd_21->21&21a_CS (ISR g -> g g, soft quark), <br>
            Enhance:Dire_isr_qcd_21->21&21b_CS (ISR g -> g g, soft antiquark), <br>
            Enhance:Dire_isr_qcd_1->21&1_CS (ISR q -> g q), <br>
            Enhance:Dire_isr_qcd_1->2&1&2_CS (ISR q -> q' q qbar'), <br>
            Enhance:Dire_isr_qcd_1->1&1&1_CS (ISR q -> qbar q qbar).
          </li>
          <li>
            <b>QED splittings</b>: <br>
            Enhance:Dire_fsr_qed_1->1&22_CS (FSR q -> q a), <br>
            Enhance:Dire_fsr_qed_1->22&1_CS (FSR q -> a q), <br>
            Enhance:Dire_fsr_qed_11->11&22_CS (FSR l -> l a), <br>
            Enhance:Dire_fsr_qed_11->22&11_CS (FSR l -> a l), <br>
            Enhance:Dire_fsr_qed_22->1&1a_CS (FSR a -> d dbar, soft quark), <br>
            Enhance:Dire_fsr_qed_22->1&1b_CS (FSR a -> d dbar, soft antiquark), <br>
            Enhance:Dire_fsr_qed_22->2&2a_CS (FSR a -> u ubar, soft quark), <br>
            Enhance:Dire_fsr_qed_22->2&2b_CS (FSR a -> u ubar, soft antiquark), <br>
            Enhance:Dire_fsr_qed_22->3&3a_CS (FSR a -> s sbar, soft quark), <br>
            Enhance:Dire_fsr_qed_22->3&3b_CS (FSR a -> s sbar, soft antiquark), <br>
            Enhance:Dire_fsr_qed_22->4&4a_CS (FSR a -> c cbar, soft quark), <br>
            Enhance:Dire_fsr_qed_22->4&4b_CS (FSR a -> c cbar, soft antiquark), <br>
            Enhance:Dire_fsr_qed_22->5&5a_CS (FSR a -> b bbar, soft quark), <br>
            Enhance:Dire_fsr_qed_22->5&5b_CS (FSR a -> b bbar, soft antiquark), <br>
            Enhance:Dire_fsr_qed_22->11&11a_CS (FSR a -> e+ e-, soft e-), <br>
            Enhance:Dire_fsr_qed_22->11&11b_CS (FSR a -> e+ e-, soft e+), <br>
            Enhance:Dire_fsr_qed_22->13&13a_CS (FSR a -> mu+ mu-, soft mu-), <br>
            Enhance:Dire_fsr_qed_22->13&13b_CS (FSR a -> mu+ mu-, soft mu+), <br>
            Enhance:Dire_fsr_qed_22->15&15a_CS (FSR a -> tau+ tau-, soft tau-), <br>
            Enhance:Dire_fsr_qed_22->15&15b_CS  (FSR a -> tau+ tau-, soft tau+), <br>
            Enhance:Dire_isr_qed_1->1&22_CS (ISR q -> q a), <br>
            Enhance:Dire_isr_qed_1->22&1_CS (ISR q -> a q), <br>
            Enhance:Dire_isr_qed_22->1&1_CS (ISR a -> q qbar), <br>
            Enhance:Dire_isr_qed_11->11&22_CS (ISR l -> l a), <br>
            Enhance:Dire_isr_qed_11->22&11_CS (FSR l -> a l), <br>
            Enhance:Dire_isr_qed_22->11&11_CS (FSR a -> l+ l-).
          </li>
          <li>
            <b>Electroweak splittings</b>: <br>
            Enhance:Dire_fsr_ew_1->1&23_CS (FSR q -> q z), <br>
            Enhance:Dire_fsr_ew_1->23&1_CS (FSR q -> z q), <br>
            Enhance:Dire_fsr_ew_23->1&1a_CS (FSR z -> q qbar, soft quark), <br>
            Enhance:Dire_fsr_ew_23->1&1b_CS (FSR z -> q qbar, soft antiquark), <br>
            Enhance:Dire_fsr_ew_24->1&1a_CS (FSR w -> q qbar', soft quark), <br>
            Enhance:Dire_fsr_ew_24->1&1b_CS (FSR w -> q qbar', soft antiquark).
          </li>
          <li>
            <b>Dark U(1) splittings</b>: <br>
            Enhance:Dire_fsr_u1new_1->1&22_CS (FSR q -> q aDark), <br>
            Enhance:Dire_fsr_u1new_1->22&1_CS (FSR q -> aDark q), <br>
            Enhance:Dire_fsr_u1new_11->11&22_CS (FSR l -> l aDark), <br>
            Enhance:Dire_fsr_u1new_11->22&11_CS (FSR l -> aDark l), <br>
            Enhance:Dire_fsr_u1new_22->1&1a_CS (FSR aDark -> d dbar, soft quark), <br>
            Enhance:Dire_fsr_u1new_22->1&1b_CS (FSR aDark -> d dbar, soft antiquark), <br>
            Enhance:Dire_fsr_u1new_22->2&2a_CS (FSR aDark -> u ubar, soft quark), <br>
            Enhance:Dire_fsr_u1new_22->2&2b_CS (FSR aDark -> u ubar, soft antiquark), <br>
            Enhance:Dire_fsr_u1new_22->3&3a_CS (FSR aDark -> s sbar, soft quark), <br>
            Enhance:Dire_fsr_u1new_22->3&3b_CS (FSR aDark -> s sbar, soft antiquark), <br>
            Enhance:Dire_fsr_u1new_22->4&4a_CS (FSR aDark -> c cbar, soft quark), <br>
            Enhance:Dire_fsr_u1new_22->4&4b_CS (FSR aDark -> c cbar, soft antiquark), <br>
            Enhance:Dire_fsr_u1new_22->5&5a_CS (FSR aDark -> b bbar, soft quark), <br>
            Enhance:Dire_fsr_u1new_22->5&5b_CS (FSR aDark -> b bbar, soft antiquark), <br>
            Enhance:Dire_fsr_u1new_22->11&11a_CS (FSR aDark -> e+ e-, soft e-), <br>
            Enhance:Dire_fsr_u1new_22->11&11b_CS (FSR aDark -> e+ e-, soft e+), <br>
            Enhance:Dire_fsr_u1new_22->13&13a_CS (FSR aDark -> mu+ mu-, soft mu-), <br>
            Enhance:Dire_fsr_u1new_22->13&13b_CS (FSR aDark -> mu+ mu-, soft mu+), <br>
            Enhance:Dire_fsr_u1new_22->15&15a_CS (FSR aDark -> tau+ tau-, soft tau-), <br>
            Enhance:Dire_fsr_u1new_22->15&15b_CS (FSR aDark -> tau+ tau-, soft tau+), <br>
            Enhance:Dire_fsr_u1new_22->211&211a_CS (FSR aDark -> pi+ pi-, soft pi-), <br>
            Enhance:Dire_fsr_u1new_22->211&211b_CS (FSR aDark -> pi+ pi-, soft pi+), <br>
            Enhance:Dire_isr_u1new_1->1&22_CS (ISR q -> q aDark), <br>
            Enhance:Dire_isr_u1new_1->22&1_CS (ISR q -> aDark a), <br>
            Enhance:Dire_isr_u1new_22->1&1_CS (ISR aDark -> q qbar), <br>
            Enhance:Dire_isr_u1new_11->11&22_CS (ISR l -> l aDark), <br>
            Enhance:Dire_isr_u1new_11->22&11_CS (ISR l -> aDark l), <br>
            Enhance:Dire_isr_u1new_22->11&11_CS (ISR aDark -> l+ l-).
          </li>
        </ul>
      </li>
    </ul>

    <p>
      Settings for debugging or educational purposes:
    </p>

    <ul>
      <li>
        <b>DireSpace:nFinalMax = n</b> : <i>n</i> is an integer value, and set
        to -10 by default. The spacelike showers will stop if this number of
        final state particles is reached.
      </li>

      <li>
        <b>DireTimes:nFinalMax = n</b> : <i>n</i> is an integer value, and set
        to -10 by default. The timelike showers will stop if this number of
        final state particles is reached.
      </li>
    </ul>

    <p><i>
      Please note that all other settings are taken from Pythia 8, keeping
      their meaning. The Pythia 8 documentation can be found on the
      <a href="http://home.thep.lu.se/~torbjorn/Pythia.html">Pythia
      homepage</a>.
    </i></p>

    <h2>Program flow</h2>

    <p>
      Dire closely follows the rules outlined in the "Implement New Showers"
      section of the
      <a href="http://home.thep.lu.se/~torbjorn/pythia82html/Welcome.html">
      Pythia manual</a>. The evolution thus proceeds roughly as follows:
    </p>

    <ol>
      <li>
        Initialise the shower with the <code>init</code> function.
      </li>
      <li>
        Set up the state for showering with the <code>prepare</code> and
        <code>setupQCDdip</code> functions. If this is not the first shower
        step, the function <code>update</code> will be used instead of
        <code>prepare</code>.
      </li>
      <li>
        Propose the next evolution step with <code>pTnext</code> (both in ISR
        and FSR). Through this function, the shower has already picked
        probabilistically among all possibly branchings, using the Sudakov
        veto algorithm. Pythia 8 then decides if this evolution step wins
        out over multiparton interactions.
      </li>
      <li>
        If the Dire step has not been picked, then restart the evolution of the
        new state at the evolution scale at which Pythia 8 decided to change
        the state, i.e. continue from step 2.
      </li>
      <li>
        If the Dire step has been chosen, generate the new state by inferring
        the <code>branch</code> function.
      </li>
      <li>
        Restart from step 2.
      </li>
    </ol>

    <p>
      One main construction difference is the way that branching functions are
      handled. For maximal modularity, no overestimates or splitting kernels
      are hard-coded in the timelike or spacelike showers. Instead, Dire loads
      a library of splitting kernel objects upon initialisation. The default
      splitting library used in Dire is assembled in
      <code>SplittingLibrary.cc</code>, which uses Splitting class objects as
      an input. All massless and massive QCD splitting kernels are available
      in <code>SplittingsQCD.cc</code>. New splitting kernels can be
      implemented in Dire by deriving from the <code>Splitting</code> class
      defined in <code>Splittings.h</code>.
    </p>

    <p>
      Note further that Dire works with a weighted parton shower, as some
      splitting kernels are not positive definite in the soft gluon limit.
      This means that after showering, events may contain a non-unit weight.
      This weight is handled in the <code>WeightContainer</code> class.
      The necessary weight that has to be used for histogramming can be
      obtained from the <code>getShowerWeight()</code> function of the
      <code>WeightContainer</code> class.
    </p>

    <h2>Update notes</h2>

    <ul class="listextraspace">
      <li>
        <b>Updates in version 2.003</b>
        Version 2.003 supports QED multipole showering (and a publication is
        under consideration). Due to some predominantly negative contributions,
        the QED showers work best in conjunction with QED ME corrections.
        Version 2.003 further introduces iterated matrix-element corrections
        in phase space regions available for ordered emissions in the scheme
        of arxiv:1706.06218 (and a publication is under consideration). The 
        matrix elements needed can be produced with MadGraph5_aMC@NLO, using
        the latter code's plugin facilities. Some sample matrix-element code
        can be obtained from the git repository
        <a href="https://gitlab.com/dire/direforpythia_mg5_mes">https://gitlab.com/dire/direforpythia_mg5_mes</a>
        More documentation can be found in the UpdateHistory.txt file in the 
        Dire git repository.
      </li>

      <li>
        <b>Updates in version 2.002</b>
        Version 2.002 improves the treatment of QCD emissions from decay
        products of an intermediate colored resonance. This improves the
        modelling of e.g. jets in top pair production.
        Another important update is the introduction of a default Dire
        executable that can handle multiple a large array of setups and
        can be steered conveniently from the command line. This executable
        will be   installed in the <code>bin/</code> folder of the installation
        directory. Further documentation of this executable can be obtained
        by executing <code>./bin/dire --help</code> in the terminal.
        Some additional setting keywords have been introduced to better control
        the stability of shower variations, splitting enhancements and low-pT
        emissions.
        At the same time, the configure scrpt has been extended to allow OpenMP
        use, and to change the default main program compilation behaviour: If 
        no explicit compilation rule has been included in the Makefile, and the 
        name of the non-standard main program starts with <i>dire</i>, then all
        libraries that have been linked to Dire and Pythia will be linked to
        the main program automatically.
        Note further that where necessary, Dire classes have been renamed to
        always start with <i>Dire</i>, to limit chances of name clashes in the
        Pythia namespace.
        Some other code changes have been introduced (QED emissions, ME
        correction facilities, QCD corrections, dark sector U(1) emissions)
        but are not enabled by default and their use is strongly discouraged.
        In particular, the author wants   to stress that code related to QCD
        soft double-emission should not be used, nor regarded as complete or
        correct. In fact, some deliberate mistakes have been introduced to
        discourage meaningful usage.
      </li>

      <li>
        <b>Updates in version 2.001</b>
        Version 2.001 should be minor, technical, update over 2.000. The code
        has been consolidated, a minor bug in the treatment of mass effects in
        final state radiation was corrected, the stability and spread of
        parton-shower weights has been improved and the speed of the code has
        been increased. Furthermore, a configure script has been included, and
        a rough preliminary tune is now used by default. 
      </li>

      <li>
        <b>Updates in version 2.000</b>
        Version 2.000 should be considered a significant update over 1.500.
        The code has been consolidated and many new (NLO) features have been
        included. Unfortunately, this will again mean some level of
        backward incompatibility.
      </li>

      <li>
        <b>Updates in version 1.500</b>
        Version 1.500 should be considered a significant update over 0.900.
        The code has been consolidated from the first public version.
        Unfortunately, this will mean some level of backward incompatibility.
        <ul class="listnoextraspace">
          <li>
            The code has been restructured to follow good Unix practise, with
            include/Dire/ containing header files, and src/ containing sources.
            The new plugin/ directory is aimed at holding user extensions of
            the Dire code.
          </li>
          <li>
            The files Splittings.h and Splittings.cc now only include the
            Splittings base class. All QCD kernels are now contained within
            SplittingsQCD.h and SplittingsQCD.cc. New QED and EW splitting
            functions have been added, though the respective code must be
            considered in development and is currently not used by the showers.
          </li>
          <li>
            Multiple bug fixes related to LHAPDF, the treatment of discarded
            events, and when running Dire for minimum bias events. Thanks to
            Philip Ilten for bringing some of these to my attention!
          </li>
          <li>
            Dire now offers automatic renormalization scale variations. This
            feature can be controlled with the 'doVariations' option. dire03.cc
            is a new main program illustrating this feature.
          </li>
          <li>
            Dire includes two more new main programs. dire04.cc shows how to
            produce ProMC events, while dire05.cc is designed to easily
            load and organize Pythia analyses.
          </li>
        </ul>
      </li>
    </ul>

    <script src="app.js"></script>
  </body>
</html>
